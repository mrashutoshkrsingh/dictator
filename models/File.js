var mongoose = require("mongoose");

var fileSchema = new mongoose.Schema({
  name: { type: String, required: true },
  text: String,
  createdAt: { type: Number, required: true },
  _creator: { type: mongoose.Schema.Types.ObjectId, required: true }
});

module.exports = File = mongoose.model("files", fileSchema);
