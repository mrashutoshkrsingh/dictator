var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  state: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  token: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now()
  },
  mobile: {
    type: Number,
    required: true
  },
  secretMsgToken: String,
  secretEmailToken: String,
  activeMail: { type: Boolean, default: false }, //will be activated by email validation after signUp
  activeMobile: { type: Boolean, default: false } //will be activated by sms validation after signUp
});

module.exports = User = mongoose.model("users", UserSchema);
