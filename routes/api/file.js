const _ = require("lodash");
var moment = require("moment");
const File = require("../../models/File");
const validateFileInput = require("../validation/register");
const { authenticate } = require("../../middleware/authenticate");
module.exports = app => {
  app.post("/file", authenticate, (req, res) => {
    const { errors, isValid } = validateFileInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const _client = req.user._id ? "" : req.user._id;
    const body = _.pick(req.body, ["name", "text"]);
    body.createdAt = moment().valueOf();
    //body._client = _client;
    new File(body)
      .save()
      .then(file => {
        res.send({ file });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //get case files of a client
  app.get("/file", authenticate, (req, res) => {
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    File.find({ _creator })
      .then(files => {
        res.send({ files });
      })
      .catch(e => {
        res.status(400).send();
      });
  });
};
