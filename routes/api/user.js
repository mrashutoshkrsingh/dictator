const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const { sendVerifyMail } = require("../../misc/sendgrid");
const axios = require("axios");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const secret = require("../../config/keys").secretKey;
//Local Input validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");

module.exports = app => {
  //user register

  app.post("/user/register", (req, res) => {
    //console.log(req.body);
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      User.findOne({
        $or: [
          { mobile: req.body.mobile },
          { email: req.body.email },
          { username: req.body.username }
        ]
      }).then(user => {
        if (user) {
          if (user.username === req.body.username)
            return res
              .status(400)
              .json({ username: "username already exists" });
          if (user.mobile === req.body.mobile)
            return res.status(400).json({ mobile: "mobile already exists" });
          if (user.email === req.body.email)
            return res.status(400).json({ email: "email already exists" });
        }
        var body = _.pick(req.body, [
          "firstName",
          "lastName",
          "email",
          "mobile",
          "password",
          "state",
          "city",
          "username"
        ]);

        body.secretEmailToken = Math.floor(Math.random() * 100000);
        body.secretMsgToken = Math.floor(Math.random() * 100000);
        const newUser = new User(body);
        axios
          .get(
            `http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=mayankmodi&password=mayank@051&sender=MANGLA&mobileno=${
              body.mobile
            }&msg=Your verification code is: ${
              body.secretMsgToken
            } to {req.headers.host}/verifyaccount/sms`
          )
          .then(res => console.log(res.data));
        const to = body.email;
        const text = `copy and paste the following code: ${
          body.secretEmailToken
        } to {req.headers.host}/verifyaccount/email`;
        const subject = "Verify your account on dictator";
        sendVerifyMail(to, subject, text);
        bcrypt.genSalt(10, function(err, salt) {
          bcrypt.hash(newUser.password, salt, function(err, hash) {
            // Store hash in your password DB.
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                res.json(user);
              })
              .catch(err => console.log(err));
          });
        });
      });
    }
  });

  //user login
  app.post("/user/login", (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      const username = req.body.username;
      const password = req.body.password;

      User.findOne({
        $or: [{ email: req.body.username }, { username: req.body.username }]
      }).then(user => {
        if (!user) return res.status(404).json({ username: "User not found" });
        if (!(user.activeMail && user.activeMobile)) {
          return res
            .status(400)
            .send({ accountActive: false, username: user.username });
        }
        bcrypt.compare(password, user.password).then(response => {
          if (!response)
            return res.status(404).json({ password: "password incorrect" });
          //user matched

          const payload = {
            id: user.id,
            firstName: user.firstName
          }; //create jwt payload

          //sign tocken
          jwt.sign(payload, secret, { expiresIn: 3600 }, (err, token) => {
            if (err) throw err;
            user.update({
              $set: {
                token: "Bearer " + token
              }
            });
            res.json({
              success: true,
              token: "Bearer " + token
            });
          });
        });
      });
    }
  });

  //reset password
  app.post("/reset/:secretToken", (req, res) => {
    const secretMsgToken = req.params.secretToken;
    User.findOne({ secretMsgToken })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }
        user.password = req.body.password;
        user.secretMsgToken = "";
        user.secretEmailToken = "";
        return user.save();
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //current user
  app.get(
    "/current",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      const { id, firstName, email } = req.user;
      res.json({ id, firstName, email });
    }
  );
};
