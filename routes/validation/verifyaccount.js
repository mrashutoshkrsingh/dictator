const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validateverifyaccount(data) {
  let errors = {};

  data.secrettoken = !isEmpty(data.secrettoken) ? data.secrettoken : "";

  if (Validator.isEmpty(data.secrettoken)) {
    errors.secrettoken = " Name field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
