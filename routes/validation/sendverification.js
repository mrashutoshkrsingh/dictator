const Validator = require("validator");
const isEmpty = require("./isEmpty");

module.exports = function validatesendverification(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : "";
  data.action = !isEmpty(data.action) ? data.action : "";

  if (Validator.isEmpty(data.email)) {
    errors.email = " email field is required";
  }

  if (Validator.isEmpty(data.action)) {
    errors.action = "action field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
